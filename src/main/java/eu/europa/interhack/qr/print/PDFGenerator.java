package eu.europa.interhack.qr.print;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFGenerator {

	public static void generate(String qrFile) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("out/AddImageExample.pdf"));
			document.open();
			document.add(new Paragraph("Name: Daniel Chirita"));
			document.add(new Paragraph("Expires: 21/07/2017"));

			Image image1 = Image.getInstance(qrFile);
			image1.scaleAbsolute(360, 360);
			document.add(image1);

			image1 = Image.getInstance("src/main/resources/logos/council.png");
			image1.scaleAbsolute(100, 100);
			document.add(image1);

			document.close();
			writer.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
