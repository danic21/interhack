package eu.europa.interhack.qr;

import java.awt.Color;
import java.io.File;
import java.util.Random;

import eu.europa.interhack.qr.decode.Decoder;
import eu.europa.interhack.qr.decode.DecodingException;
import eu.europa.interhack.qr.encode.Encoder;
import eu.europa.interhack.qr.encode.EncodingException;
import eu.europa.interhack.qr.encode.EncodingOptions;
import eu.europa.interhack.qr.encode.EncodingOptions.ImageType;
import eu.europa.interhack.qr.print.PDFGenerator;

public class Generator {

	public static void main(String[] args) throws EncodingException, DecodingException {
		String name = "" + new Random().nextInt(99);

		EncodingOptions options = new EncodingOptions();
		options.setSize(340);
		options.setImageType(ImageType.PNG);
		options.setMargin(1);
		options.setForegroundColor(Color.BLACK);

		String content = "Daniel Chiriță: Lorem ipsum dolor sit amet consectetuer Pellentesque eleifend dictumst odio pede. Enim convallis sollicitudin volutpat Sed consectetuer convallis sollicitudin ";
		File out = Encoder.encode(content, name, "e:/tools/ws/qr/out/", options);

		String fileName = out.getAbsolutePath();
		System.out.println(fileName);
		String result = Decoder.decode(fileName);
		System.out.println(result);

		assert (result.compareTo(content) == 0);

		PDFGenerator.generate(fileName);
	}
}
